$(document).ready(function() {
    $.extend(danceword,{loop:true})
    $('#superContainer').fullpage({
        navigation: true,
        continuousVertical: true
    });
    function getOs(){
       var OsObject = "";
       if(navigator.userAgent.indexOf("MSIE")>0) {
            return "MSIE";
       }
       if(isFirefox=navigator.userAgent.indexOf("Firefox")>0){
            $.getScript('./js/mousefollow.js');
            return "Firefox";
       }
       if(isSafari=navigator.userAgent.indexOf("Safari")>0) {
           $.getScript('./js/mousefollow.js');
           return "Safari";
       }
       if(isCamino=navigator.userAgent.indexOf("Camino")>0){
            $.getScript('./js/mousefollow.js');
            return "Camino";
       }
       if(isMozilla=navigator.userAgent.indexOf("Gecko/")>0){
            $.getScript('./js/mousefollow.js');
            return "Gecko";
       }
       if(isChrome=navigator.userAgent.indexOf("Chrome/")>0){
            $.getScript('./js/mousefollow.js');
            return "Chrome";
       }
    }
    getOs();
    // // 花瓣飘落
    // $(document).snowfall('clear');
    // $(document).snowfall({
    //     image: "images/flower.png",
    //     flakeCount: 16,
    //     minSize: 6,
    //     maxSize: 25
    // });
    // 万年历
    Date.prototype.Format = function (fmt) { //author: meizz
        var o = {
            "M+": this.getMonth() + 1, //月份
            "d+": this.getDate(), //日
            "h+": this.getHours(), //小时
            "m+": this.getMinutes(), //分
            "s+": this.getSeconds(), //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds() //毫秒
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }
    
    function calendar () {
        var date = new Date().Format("yyyy-MM-dd");
        var url = 'http://api.jisuapi.com/calendar/query';
        var data = {
            date: date,
            appkey: 'de9653892eba48b8'
        }
        var calendarData = localStorage.getItem("calendarData");
        if (!!calendarData) {
            calendarData = JSON.parse(calendarData);
        }
        if (!calendarData || (!!calendarData && parseInt(calendarData.day) !== new Date().getDate())) {
            $.ajax({
                 type: "get",
                 url: url,
                 data: data,
                 dataType: 'jsonp',
                 success: function(res){
                     if (res.status == 0) {
                         localStorage.setItem('calendarData', JSON.stringify(res.result));
                         setCalendar(res.result);
                     }
                 },
                 error: function(){
                     alert('fail');
                 }
             });
        }else {
            setCalendar(calendarData)
        }
        // var json = {
        //     year: "2017",
        //     month: "02",
        //     day: "14",
        //     week: "二",
        //     lunaryear: "2017",
        //     lunarmonth: "正月",
        //     lunarday: "十八",
        //     ganzhi: "丁酉",
        //     shengxiao: "鸡",
        //     festival: {
        //         solar: '情人節'
        //     },
        //     "huangli": {
        //         "nongli": "农历二〇一五年九月初十",
        //         "taishen": "厨灶厕外西南",
        //         "wuxing": "路旁土",
        //         "chong": "冲（乙丑）牛",
        //         "sha": "煞西",
        //         "jiri": "朱雀（黑道）收日",
        //         "zhiri": "朱雀（黑道凶日）",
        //         "xiongshen": "地曩 月刑 河魁 五虚 朱雀",
        //         "jishenyiqu": "天德合 母仓 不将 玉宇 月德合",
        //         "caishen": "正东",
        //         "xishen": "西南",
        //         "fushen": "西南",
        //         "suici": [
        //             "乙未年",
        //             "丙戌月",
        //             "辛未日"
        //         ],
        //         "yi": [
        //             "祭祀",
        //             "冠笄",
        //             "移徙",
        //             "会亲友",
        //             "纳财",
        //             "理发",
        //             "捕捉"
        //         ],
        //         "ji": [
        //             "嫁娶",
        //             "开市",
        //             "开池",
        //             "作厕",
        //             "破土"
        //         ]
        //     }
        // };
        // setCalendar(json);
    }
    function setCalendar(json) {
        var ganzhi = $('.calendar-ganzhi');
        ganzhi.html(json.ganzhi + '年 ' + json.lunaryear);
        var shengxiao = $('.calendar-zodiac');
        shengxiao.html(json.shengxiao);
        var day = $('.calendar-day');
        day.html(json.day);
        var month = $('.calendar-month');
        month.html(json.month);
        var lunar = $('.calendar-lunar');
        lunar.html(json.lunarmonth + ' ' + json.lunarday + ' 星期' + json.week + ' ' + (json.festival && json.festival.solar || ''));
        var taboo  = $('.calendar-taboo');
        var ji = json.huangli.ji;
        if (ji.length > 5) {
            ji = ji.slice(0, 5);
        }
        taboo.html('忌: ' + ji.join(' '));
        var benefit  = $('.calendar-benefit');
        var yi = json.huangli.yi;
        if (yi.length > 5) {
            yi = yi.slice(0, 5);
        }
        var yiHtml = '';
        for (var i = 0; i < yi.length; i++) {
            yiHtml += '<li>' + yi[i] + '</li>';
        }
        benefit.html(yiHtml);
    }
    calendar();
    // 點擊查看更多工作經歷
    $('.work-more').click(function  () {
        var $el = $(this);
        var openSatate = $el.attr('open-state') || 0;
        var workBoxEl = $el.parents('.work-box');
        var workBoxElTop;
        if (workBoxEl.index() === 2) {
            workBoxElTop = parseInt(workBoxEl.css('top')) /12;
        }
        if (openSatate == 1) {
            $el.attr('open-state', 0);
            $el.html('展開').css({
                'bottom': '-5em'
            }).siblings('.work-content').css({
                '-webkit-line-clamp': '1'
            }).parents('.work-box').css({
                'top': workBoxElTop + 6 + 'em'
            }).siblings('.work-box').show();
        }else {
            $el.attr('open-state', 1);
            $el.html('收起').css({
                'bottom': '-12.5em'
            }).siblings('.work-content').css({
                '-webkit-line-clamp': '10'
            }).parents('.work-box').css({
                'top': workBoxElTop - 6 + 'em'
            }).siblings('.work-box').hide();
        }
        // $el.parents('.work-box').siblings('.work-box').find('.work-content').css({
        //     '-webkit-line-clamp': '1'
        // }).siblings('.work-more').show();
    });
});
